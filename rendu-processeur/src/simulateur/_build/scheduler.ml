open Netlist_ast
open Graph

exception Combinational_cycle

let read_exp eq =
  let getid = function
    | Avar(i) -> [i]
    | _ -> [] in
  match eq with
    (_, e) -> begin
      match e with
      | Earg(a) -> getid a
      | Ereg(i) -> []
      | Enot(a) -> getid a
      | Ebinop(bop, a1, a2) -> (getid a1) @ (getid a2)
      | Emux(a1, a2, a3) -> (getid a1) @ (getid a2) @ (getid a3)
      | Erom(_, _, rdaddr) -> getid rdaddr
      | Eram(_, _, rdaddr, wren, wraddr, data)
        -> (getid rdaddr) (*@ (getid wren) @ (getid wraddr) @ (getid data)*)
      | Econcat(a1, a2) -> (getid a1) @ (getid a2)
      | Eslice(_, _, a) -> getid a
      | Eselect(_, a) -> getid a
    end

let schedule p =
  let p_l = List.map read_exp p.p_eqs in
  let p_i = List.map (fun (i, _) -> i) p.p_eqs in
  let prg = Array.of_list p.p_eqs in
  let eqs = Hashtbl.create 97 in
  let n = List.fold_left (fun c i -> (Hashtbl.add eqs i c); c+1) 0 p_i in
  (*let eqs_set = ref (Array.make n true) in*)
  let arr = Array.make n [] in
  let _ = List.fold_left (fun c l
                          -> List.iter (fun i
                                        -> arr.(c) <- (Hashtbl.find_all eqs i)@(arr.(c))) l ;
                             c+1) 0 p_l in
  (*let arr_filtre = Array.map (fun l
                              -> let new_l = List.filter (fun i
                                                          -> let b = !eqs_set.(i) in
                                                             if b then !eqs_set.(i) <- false;
                                                             b) l in
                                 eqs_set := Array.map (fun _ -> true) !eqs_set ;
                                 new_l) arr in*)
  let g = Graph.graph_of_graph' arr(*_filtre*) in
  if Graph.has_cycle g then raise Combinational_cycle ;
  let t = Graph.topological g in
  { p_eqs = List.rev(List.map (fun i -> prg.(i)) t) ;
    p_inputs = p.p_inputs ;
    p_outputs = p.p_outputs ;
    p_vars = p.p_vars } ;;
