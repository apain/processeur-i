{
  open Lexing
  open Ast
  open Parser

  exception Lexing_error of string

  let new_line lexbuf =
          let pos = lexbuf.lex_curr_p in
          lexbuf.lex_curr_p <-
                { pos with pos_bol = lexbuf.lex_curr_pos;
                  pos_lnum = pos.pos_lnum + 1 
                }

}

let digit = ['0'-'9']
let integer = '0' | '-'? ['1'-'9'] digit*
let space = ' ' | '\t'
let letter = ['a'-'z' 'A'-'Z']
let label = (letter | '_') (letter | digit | '_')*

rule next_tokens = parse
  | '\n'        { new_line lexbuf; next_tokens lexbuf }
  | space+      { next_tokens lexbuf }
  | "(*"        { comment lexbuf }
  | eof         { [EOF] }
  | "#"         { [TAG] }
  | ","         { [SEP] }
  | "("         { [LP] }
  | ")"         { [RP] }
  | "add"       { [ADD] }
  | "addi"      { [ADDI] }
  | "sub"       { [SUB] }
  | "subi"      { [SUBI] }
  | "and"       { [AND] }
  | "andi"      { [ANDI] }
  | "or"        { [OR] }
  | "ori"       { [ORI] }
  | "xor"       { [XOR] }
  | "xori"      { [XORI] }
  | "slt"       { [SLT] }
  | "slti"      { [SLTI] }
  | "sltu"      { [SLTU] }
  | "sltiu"     { [SLTIU] }
  | "beq"       { [BEQ] }
  | "bne"       { [BNE] }
  | "blt"       { [BLT] }
  | "bltu"      { [BLTU] }
  | "bge"       { [BGE] }
  | "bgeu"      { [BGEU] }
  | "jal"       { [JAL] }
  | "jarl"      { [JARL] }
  | "sw"        { [SW] }
  | "lw"        { [LW] }
  | "mv"        { [MV] }
  | "li"        { [LI] }
  | "j"         { [J] }
  | "not"       { [NOT] }
  | "incr"      { [INCR] }
  | "nop"       { [NOP] }
  | "x0"        { [REG 0] }
  | "x1"        { [REG 1] }
  | "x2"        { [REG 2] }
  | "x3"        { [REG 3] }
  | "x4"        { [REG 4] }
  | "x5"        { [REG 5] }
  | "x6"        { [REG 6] }
  | "x7"        { [REG 7] }
  | "x8"        { [REG 8] }
  | "x9"        { [REG 9] }
  | "x10"       { [REG 10] }
  | "x11"       { [REG 11] }
  | "x12"       { [REG 12] }
  | "x13"       { [REG 13] }
  | "x14"       { [REG 14] }
  | "x15"       { [REG 15] }
  | integer as s
                { try [IMM (int_of_string s)]
                with _ -> raise (Lexing_error ("constant too large: " ^ s)) }
  | label as l   { [LABEL l] }
  | _ as c      { raise (Lexing_error ("illegal character: " ^ String.make 1 c)) }

and comment = parse
  | "*)"        { next_tokens lexbuf }
  | '\n'        { new_line lexbuf; comment lexbuf }
  | _           { comment lexbuf }
  | eof         { raise (Lexing_error ("unfinished comment")) }

{
  let next_token =
    let tokens = Queue.create () in 
    fun lb ->
      if Queue.is_empty tokens then begin
	let l = next_tokens lb in
	List.iter (fun t -> Queue.add t tokens) l
      end;
      Queue.pop tokens
}

