open Ast
open Format

exception Compile_error of loc * string

let lab_tbl = Hashtbl.create 17

(* binaire: poids fort à gauche *)

let bin_of_int d nb_bits =
        let rec aux acc d nb_bits =
                if nb_bits = 0 then acc else
                aux (string_of_int (d land 1) :: acc) (d lsr 1) (nb_bits - 1)
        in
        String.concat "" (aux [] d nb_bits)

let write_bin4 x = 
        bin_of_int x 4

let write_bin12 x =
        bin_of_int x 12

let write_bin16 x =
        bin_of_int x 16

let bin_code r =
        let Reg x = r in
        write_bin4 x

let get_ofs i label = 
        (* calculates the difference between current instruction and target instruction (pointed by label) *)
        begin try (let label_i = Hashtbl.find lab_tbl label in
        label_i - i) 
        with Not_found -> failwith ("label " ^ label ^ " not defined yet") end

let type_code_R = "00"
let type_code_I = "01"
let type_code_U = "10"
let type_code_S = "11"

let instr_code_R opR =
        let instr_code opR = 
                begin match opR with
                | Radd   -> "0100000001"
                | Rsub   -> "0100000101"
                | Rand   -> "0100000000"
                | Ror    -> "0100000010"
                | Rxor   -> "0100001001"
                | Rslt   -> "0100000111"
                | Rsltu  -> "0100001111"
                | Rjarl  -> "1100000001"
                end
        in type_code_R ^ instr_code opR

let instr_code_I opI =
        let instr_code opI =
                begin match opI with
                | Iaddi  -> "0100100001"
                | Isubi  -> "0100100101"
                | Iandi  -> "0100100000"
                | Iori   -> "0100100010"
                | Ixori  -> "0100101001"
                | Islti  -> "0100100111"
                | Isltiu -> "0100101111"
                | Ilw    -> "0101100001"
                end
        in type_code_I ^ instr_code opI

let instr_code_S opS =
        let instr_code opS =
                begin match opS with
                | Sbeq   -> "1000001101"
                | Sbne   -> "1000000101"
                | Sblt   -> "1000000111"
                | Sbltu  -> "1000001111"
                | Sbge   -> "1000010111"
                | Sbgeu  -> "1000011111"
                | Ssw    -> "0010100001"
                end
        in type_code_S ^ instr_code opS

let instr_code_U opU =
        let instr_code opU = "1100100001"
        in type_code_U ^ instr_code opU

let fill_lab_tbl = function
        | Label (label, i) -> Hashtbl.add lab_tbl label i
        | _ -> ()

let rec compile_instr = function
        | TypeR (_, opR, rd, rs1, rs2) ->
                "00000000" ^ bin_code rs2 ^ bin_code rs1 ^ bin_code rd ^ instr_code_R opR
        | TypeI (_, opI, rd, rs1, Imm imm) ->
                write_bin12 imm ^ bin_code rs1 ^ bin_code rd ^ instr_code_I opI
        | TypeS (_, opS, rs1, rs2, Imm imm) ->
                let str_imm = write_bin12 imm in
                String.sub str_imm 0 8 ^ bin_code rs2 ^ bin_code rs1 ^ String.sub str_imm 8 4 ^ instr_code_S opS
        | TypeU (_, opU, rd, Imm imm) ->
                write_bin16 imm ^ bin_code rd ^ instr_code_U opU
        | Jump (i, opU, rd, label) ->
                write_bin16 (get_ofs i label) ^ bin_code rd ^ instr_code_U opU
        | CondJump (i, opS, rs1, rs2, label) -> let ofs = write_bin12 (get_ofs i label) in 
                String.sub ofs 0 8 ^ bin_code rs2 ^ bin_code rs1 ^ String.sub ofs 8 4 ^ instr_code_S opS
        | TypeP i -> compile_instr i
        | _ -> ""

let reverse s =
        let rec helper i =
                if i >= String.length s then "" else (helper (i+1))^(String.make 1 s.[i])
        in
                helper 0

let compile_file ast_file out_file rom_sz =
        List.iter (fun i -> fill_lab_tbl i) ast_file;
        let p =
        List.fold_left 
        (fun str instr -> begin match instr with
        | Label _ -> str
        | _ -> let code = reverse (compile_instr instr) in str ^ code ^ "\n"
        end)
        (if rom_sz then "256;32\n" else "65536;32\n")
        ast_file
        in
        let f = open_out out_file in

        let fmt = formatter_of_out_channel f in
        fprintf fmt "%s" p;
        fprintf fmt "@?";
        close_out f
