type loc = Lexing.position * Lexing.position

type i = int

type opR = Radd | Rsub | Rand | Ror | Rxor | Rslt | Rsltu | Rjarl

type opI = Iaddi | Isubi | Iandi | Iori | Ixori | Islti | Isltiu | Ilw

type opS = Sbeq | Sbne | Sblt | Sbltu | Sbge | Sbgeu | Ssw

type opU = Ujal 

type file = 
        instruction list

and instruction =
        | TypeR of i * opR * reg * reg * reg (* rd, rs1, rs2 *)
        | TypeI of i * opI * reg * reg * imm (* rd, rs1, imm *)
        | TypeS of i * opS * reg * reg * imm (* rs1, rs2, imm *)
        | TypeU of i * opU * reg * imm (* rd, imm *)
        | TypeP of instruction 
        | Jump of i * opU * reg * string
        | CondJump of i * opS * reg * reg * string
        | Label of string * int
        
and reg =
        Reg of int

and imm =
        Imm of int
