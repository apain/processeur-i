
(* The type of tokens. *)

type token = 
  | XORI
  | XOR
  | TAG
  | SW
  | SUBI
  | SUB
  | SLTU
  | SLTIU
  | SLTI
  | SLT
  | SEP
  | RP
  | REG of (int)
  | ORI
  | OR
  | NOT
  | NOP
  | MV
  | LW
  | LP
  | LI
  | LABEL of (string)
  | JARL
  | JAL
  | J
  | INCR
  | IMM of (int)
  | EOF
  | BNE
  | BLTU
  | BLT
  | BGEU
  | BGE
  | BEQ
  | ANDI
  | AND
  | ADDI
  | ADD

(* This exception is raised by the monolithic API functions. *)

exception Error

(* The monolithic API. *)

val file: (Lexing.lexbuf -> token) -> Lexing.lexbuf -> (Ast.file)
