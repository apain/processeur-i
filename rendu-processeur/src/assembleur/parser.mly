%{
  open Ast

  let i_nb = ref 0
  let new_i () = 
          incr i_nb; !i_nb
  let same_i () = 
          !i_nb + 1 (* label has number of following instruction *)
%}

%token <int> REG
%token <int> IMM
%token <string> LABEL
%token EOF SEP LP RP 
%token TAG
%token ADD SUB AND OR XOR SLT SLTU (* type R *)
%token JARL
%token ADDI SUBI ANDI ORI XORI SLTI SLTIU (* type I 1 *)
%token LW (* type I 2 *)
%token BEQ BNE BLT BLTU BGE BGEU (* type S 1 *)
%token SW (* type S 2 *)
%token JAL (* type U *)
%token MV LI J NOT INCR NOP (* pseudo-instructions *)

%start file
%type <Ast.file> file

%%

file:
        i_list = instruction*
        EOF
        { i_list }

instruction:
        | o = opR rd = reg SEP rs1 = reg SEP rs2 = reg          { TypeR (new_i (), o, rd, rs1, rs2) }
        | JARL rd = reg SEP rs2 = reg                           { TypeR (new_i (), Rjarl, rd, Reg 0, rs2) }
        | LW rd = reg SEP i = imm LP rs1 = reg RP               { TypeI (new_i (), Ilw, rd, rs1, i) }
        | o = opI rd = reg SEP rs1 = reg SEP i = imm            { TypeI (new_i (), o, rd, rs1, i) }
        | SW i = imm LP rs1 = reg RP SEP rs2 = reg              { TypeS (new_i (), Ssw, rs1, rs2, i) }
        | o = opS rs1 = reg SEP rs2 = reg SEP i = imm           { TypeS (new_i (), o, rs1, rs2, i) }   
        | o = opS rs1 = reg SEP rs2 = reg SEP l = LABEL         { CondJump (new_i (), o, rs1, rs2, l) }
        | o = opU rd = reg SEP i = imm                          { TypeU (new_i (), o, rd, i) }
        | p = pseudo_instr                                      { TypeP p }
        | TAG l = LABEL                                         { Label (l, same_i ()) }

pseudo_instr:
        | MV rd = reg SEP rs1 = reg                             { TypeI (new_i (), Iaddi, rd, rs1, Imm 0) }
        | LI rd = reg SEP i = imm                               { TypeI (new_i (), Iaddi, rd, Reg 0, i) }
        | J i = imm                                             { TypeU (new_i (), Ujal, Reg 0, i) }
        | J l = LABEL                                           { Jump (new_i (), Ujal, Reg 0, l) }
        | NOT rd = reg SEP rs1 = reg                            { TypeI (new_i (), Ixori, rd, rs1, Imm (-1)) }
        | INCR rd = reg                                         { TypeI (new_i (), Iaddi, rd, rd, Imm 1) }
        | NOP                                                   { TypeI (new_i (), Iaddi, Reg 0, Reg 0, Imm 0) }

opR:
        | ADD           { Radd }
        | SUB           { Rsub }
        | AND           { Rand }
        | OR            { Ror }
        | XOR           { Rxor }
        | SLT           { Rslt }
        | SLTU          { Rsltu }

opI:
        | ADDI          { Iaddi }
        | SUBI          { Isubi }
        | ANDI          { Iandi }
        | ORI           { Iori }
        | XORI          { Ixori }
        | SLTI          { Islti }
        | SLTIU         { Isltiu }

opS:
        | BEQ           { Sbeq }
        | BNE           { Sbne }
        | BLT           { Sblt } 
        | BLTU          { Sbltu }
        | BGE           { Sbge }
        | BGEU          { Sbgeu }

opU:
        | JAL           { Ujal }

reg:
        r = REG         { Reg r }

imm:
        i = IMM         { Imm i }
