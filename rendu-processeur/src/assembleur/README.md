`make` creates the executable `./iasm` which takes a `.i` file as argument and produces a `.bin` file. Example files are provided in the `/tests` subdirectory.

Usage: `./iasm [file].i`
