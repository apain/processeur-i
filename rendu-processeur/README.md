# Mode d'emploi : compilation et exécution

## Compilation

Dans le dossier courant :

- `make`

Cette commande synthétise les netlists du processeur 16 bits et du processeur 32 bits et les ordonne topologiquement, compile le compilateur d'assembleur, le simulateur de netlists, l'affichage et le compilateur de minijazz.

## Lancement de l'horloge

Ces commandes compilent le code assembleur de l'horloge en un code binaire et lancent la simulation avec le fichier produit en ROM.

- Mode rapide : `make clock_fast` (lancée sur le processeur 16 bits)

- Temps réel : `make clock_rt` (lancée sur le processeur 16 bits)

- Processeur 32 bits : `make clock_slow`

## Lancement de l'affichage

- `./aff` ou `./aff2` à exécuter en parallèle de l'horloge

## Clean

- `make clean` remet le projet à l'état de rendu, sauf pour les fichiers `.net` qui ne sont éliminés que sur la commande `make clean_netlists`.
