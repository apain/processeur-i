(* CODE DE L'AFFICHAGE ET REGISTRES ASSOCIES

  0
6   1
  2
5   3
  4

0 = 1101111 = 111 x10
1 = 0101000 = 40  x11
2 = 1110110 = 118 x12
3 = 1111100 = 124 x13
4 = 0111001 = 57  x14
5 = 1011101 = 93  x15
6 = 1011111 = 95  x6
7 = 1101000 = 104 x7
8 = 1111111 = 255 x8
9 = 1111101 = 125 x9
*)

(* MEMOIRE STOCKANT LES CHIFFRES ACTUELS
s1 = x1
s2 = x2
m1 = x4
m2 = RAM 6
h1 = RAM 7
h2 = RAM 8

  MEMOIRE TEMPORAIRE
tmp : x5

  SIGNAL D'ARRET DE LA SIMULATION
wait = RAM 15
*)

(* INITIALISATION DU CODE DE CHAQUE CHIFFRE POUR AFFICHAGE *)

li x10, 111
li x11, 40
li x12, 118
li x13, 124
li x14, 57
li x15, 93
li x6,  95
li x7,  104
li x8,  255
li x9,  125

(* MISE A ZERO DE TOUS LES CHIFFRES AFFICHES - REMISE A MINUIT *)
#RESET
sw 0(x0), x10
sw 1(x0), x10
sw 2(x0), x10
sw 3(x0), x10
sw 4(x0), x10
sw 5(x0), x10

(* DEBUT DES BOUCLES D'INCREMENT DE L'HORLOGE *)
#SEC1 
li   x5,  1
sw 15(x0), x5

#SEC0
incr x1
add  x5,  x1,  x1
incr x5
jarl x0,  x5

#SEC1_0
sw 0(x0), x10
j SEC2

sw 0(x0), x11
j SEC0

sw 0(x0), x12
j SEC0

sw 0(x0), x13
j SEC0

sw 0(x0), x14
j SEC0

sw 0(x0), x15
j SEC0

sw 0(x0), x6
j SEC0

sw 0(x0), x7
j SEC0

sw 0(x0), x8
j SEC0

sw 0(x0), x9
j SEC0

li   x1,  0
j SEC1_0

#SEC2
incr x2
add  x5,  x2,  x2
incr x5
jarl x0,  x5

#SEC2_0
sw 1(x0), x10
j MIN1

sw 1(x0), x11
j SEC1

sw 1(x0), x12
j SEC1

sw 1(x0), x13
j SEC1

sw 1(x0), x14
j SEC1

sw 1(x0), x15
j SEC1

li   x2,  0
j SEC2_0

#MIN1
incr x3
add  x5,  x3,  x3
incr x5
jarl x0,  x5

#MIN1_0
sw 2(x0), x10
j MIN2

sw 2(x0), x11
j SEC1

sw 2(x0), x12
j SEC1

sw 2(x0), x13
j SEC1

sw 2(x0), x14
j SEC1

sw 2(x0), x15
j SEC1

sw 2(x0), x6
j SEC1

sw 2(x0), x7
j SEC1

sw 2(x0), x8
j SEC1

sw 2(x0), x9
j SEC1

li   x3,  0
j MIN1_0

#MIN2
lw   x5,  6(x0)
incr x5
sw 6(x0), x5
add  x5,  x5,  x5
incr x5
jarl x0,  x5

#MIN2_0
sw 3(x0), x10
j HR1

sw 3(x0), x11
j SEC1

sw 3(x0), x12
j SEC1

sw 3(x0), x13
j SEC1

sw 3(x0), x14
j SEC1

sw 3(x0), x15
j SEC1

li   x5,  0
sw 6(x0), x5
j MIN2_0

#HR1
lw   x5,  7(x0)
incr x5
sw 7(x0), x5
add  x5,  x5,  x5
incr x5
jarl x0,  x5

#HR1_0
sw 4(x0), x10
j HR2

sw 4(x0), x11
j SEC1

sw 4(x0), x12
j SEC1

sw 4(x0), x13
j SEC1

sw 4(x0), x14
j JR_TEST

sw 4(x0), x15
j SEC1

sw 4(x0), x6
j SEC1

sw 4(x0), x7
j SEC1

sw 4(x0), x8
j SEC1

sw 4(x0), x9
j SEC1

li   x5,  0
sw 7(x0), x5
j HR1_0

#JR_TEST
lw   x5,  8(x0)
subi x5,  x5,  2
bne  x0,  x5,  SEC1
li   x1,  0
li   x2,  0
li   x3,  0
sw 6(x0), x0
sw 7(x0), x0
sw 8(x0), x0
j RESET

#HR2
lw   x5,  8(x0)
incr x5
sw 8(x0), x5
add  x5,  x5,  x5
incr x5
jarl x0,  x5

#HR2_0
sw 5(x0), x10
nop

sw 5(x0), x11
j SEC1

sw 5(x0), x12
j SEC1
