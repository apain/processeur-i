(* secondes[0] = ram[0] 
secondes[1] = ram[1]
minutes[0] = ram[2]
minutes[1] = ram[3]
heures[0] = ram[4] 
heures[1] = ram[5]
x4 = 60
x5 = 24 

  0
6   1
  2
5   3    
  4

0 = 1101111 = 111 x10
1 = 0101000 = 40 x11
2 = 1110110 = 118 x12
3 = 1111100 = 124 x13
4 = 0111001 = 57 x14
5 = 1011101 = 93 x15
6 = 1011111 = 95 x6
7 = 1101000 = 104 x7
8 = 1111111 = 255 x8
9 = 1111101 = 125 x9

*)

(* un registre par chiffre 
secondes[0] = x1
secondes[1] = x2
minutes[0] = x4
*)

addi x10, x0, 111
addi x11, x0, 40
addi x12, x0, 118
addi x13, x0, 124
addi x14, x0, 57
addi x15, x0, 93
addi x6, x0, 95
addi x7, x0, 104
addi x8, x0, 255
addi x9, x0, 125

sw 0(x0), x10
sw 1(x0), x10
sw 2(x0), x10
sw 3(x0), x10
sw 4(x0), x10
sw 5(x0), x10

#SEC1
incr x1
li x3, 4
blt x1, x3, SEC1_0123 (* if x1 < 4 *)
li x3, 6
blt x1, x3, SEC1_45 (* if x1 < 6 *)
li x3, 8
blt x1, x3, SEC1_67 (* if x1 < 8 *)
beq x1, x3, SEC1_8 (* if x1 == 8 *)
sw 0(x0), x9 (* 9 *)
j TEST_SEC2

#SEC1_0123
li x3, 2
blt x1, x3, SEC1_01 (* if x1 < 2 *)
beq x1, x3, SEC1_2 (* if x1 == 2 *)
sw 0(x0), x13 (* 3 *)
j TEST_SEC2

#SEC1_01
beq x1, x0, SEC1_0 (* if x1 == 0 *)
sw 0(x0), x11 (* 1 *)
j TEST_SEC2

#SEC1_0
sw 0(x0), x10 (* 0 *)
j TEST_SEC2

#SEC1_2
sw 0(x0), x12 (* 2 *)
j TEST_SEC2

#SEC1_45
li x3, 4
beq x1, x3, SEC1_4 (* if x1 == 4 *)
sw 0(x0), x15 (* 5 *)
j TEST_SEC2

#SEC1_4
sw 0(x0), x14 (* 4 *)
j TEST_SEC2

#SEC1_67
li x3, 6
beq x1, x3, SEC1_6 (* if x1 == 6 *)
sw 0(x0), x7 (* 7 *)
j TEST_SEC2

#SEC1_6
sw 0(x0), x6 (* 6 *)
j TEST_SEC2

#SEC1_8
sw 0(x0), x8 (* 8 *)
j TEST_SEC2

#TEST_SEC2
li x3, 10
beq x1, x3, SEC1_zero (* if x1 = 10 *)
j SEC1

#SEC1_zero
li x1, 0
j SEC1



(*
incr x1 
bne x1, x4, -1 
incr x2
mv x1, x0
bne x2, x4, -4 
incr x3
mv x2, x0
bne x3, x5, -7 
mv x3, x0
j -9 
*)
