(* secondes[0] = ram[0] 
secondes[1] = ram[1]
minutes[0] = ram[2]
minutes[1] = ram[3]
heures[0] = ram[4] 
heures[1] = ram[5]
x4 = 60
x5 = 24 

  0
6   1
  2
5   3    
  4

0 = 1101111 = 111 x10
1 = 0101000 = 40 x11
2 = 1110110 = 118 x12
3 = 1111100 = 124 x13
4 = 0111001 = 57 x14
5 = 1011101 = 93 x15
6 = 1011111 = 95 x6
7 = 1101000 = 104 x7
8 = 1111111 = 255 x8
9 = 1111101 = 125 x9

*)

(* un registre par chiffre 
secondes[0] = x1
secondes[1] = x2
minutes[0] = x4
*)

addi x10, x0, 111
addi x11, x0, 40
addi x12, x0, 118
addi x13, x0, 124
addi x14, x0, 57
addi x15, x0, 93
addi x6, x0, 95
addi x7, x0, 104
addi x8, x0, 255
addi x9, x0, 125

sw 0(x0), x10
sw 1(x0), x10
sw 2(x0), x10
sw 3(x0), x10
sw 4(x0), x10
sw 5(x0), x10

#SEC1
incr x1
incr x1
incr x1
sw 6(x0), x1
addi x3, x0, 4
sw 7(x0), x3
blt x1, x3, SEC1_0123 (* if x1 < 4 *)
sw 8(x0), x1
j END

#SEC1_0123
addi x3, x0, 2
sw 9(x0), x3
sw 10(x0), x1
beq x1, x3, SEC1_2 (*if x1 == 2 *)
sw 11(x0), x1
blt x1, x3, SEC1_01 (*if x1 < 2*)
sw 12(x0), x1
sw 0(x0), x13 (* 3 *)
j SEC1

#SEC1_01
sw 13(x0), x1
beq x1, x0, SEC1_0 (* if x1 == 0 *)
sw 14(x0), x1
sw 0(x0), x11 (* 1 *)
j SEC1

#SEC1_0
sw 15(x0), x1
sw 0(x0), x10 (* 0 *)
j SEC1

#SEC1_2
sw 16(x0), x1
sw 0(x0), x12 (* 2 *)
j SEC1

#END
sw 17(x0), x1
