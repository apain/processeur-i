\beamer@sectionintoc {1}{Architecture}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Spécifications}{2}{0}{1}
\beamer@subsectionintoc {1}{2}{Pour aller plus vite}{3}{0}{1}
\beamer@subsectionintoc {1}{3}{Architecture générale}{4}{0}{1}
\beamer@sectionintoc {2}{Format des instructions}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Types d'instruction}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{Codage des instructions}{6}{0}{2}
\beamer@sectionintoc {3}{Jeu d'instructions}{7}{0}{3}
\beamer@subsectionintoc {3}{1}{Opérations arithmétiques et logiques}{7}{0}{3}
\beamer@subsectionintoc {3}{2}{Branchement conditionnel}{8}{0}{3}
\beamer@subsectionintoc {3}{3}{Branchement inconditionnel}{9}{0}{3}
\beamer@subsectionintoc {3}{4}{Lecture et écriture en mémoire}{10}{0}{3}
\beamer@subsectionintoc {3}{5}{Pseudo-instructions}{11}{0}{3}
\beamer@sectionintoc {4}{Programme assembleur}{12}{0}{4}
\beamer@sectionintoc {5}{Code de l'horloge}{13}{0}{5}
