\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{multirow}
\usepackage[a4paper, margin=1in]{geometry}
\usepackage{graphicx}

\title{Réalisation d'un micro-processeur, partie 2}
\author{Luis Kuffner et Alice Pain}
\date{Janvier 2022}

\begin{document}

\maketitle

\begin{abstract}
Ce rapport présente l'architecture et le jeu d'instruction de notre micro-processeur, ainsi que le programme assembleur d'horloge qui en fait la démonstration. 
\end{abstract}

\section{Architecture}

\subsection{Spécifications}

Nous avons choisi de concevoir un micro-processeur inspiré de l'architecture RISC-V. Voici les spécifications de notre micro-processeur :

\paragraph{Mémoire ROM :} code du programme
\begin{itemize}
  \item Taille des données (instructions du programme) : 32 bits
  \item Taille des adresses : 16 bits
\end{itemize}

\paragraph{Mémoire RAM :} mémoire du programme
\begin{itemize}
  \item Taille des données : 32 bits
  \item Taille des adresses : 16 bits
\end{itemize}

\paragraph{Registres :}
\begin{itemize}
  \item Nombre : 16 (numérotés de \texttt{x0} à \texttt{x15})
  \item Taille : 32 bits
  \item Le registre \texttt{x0} contient toujours la valeur 0.
\end{itemize}

\subsection{Architecture générale}

\begin{figure}
	\centering
	\includegraphics[scale=0.27]{architecture.png}
	\caption{Schéma général de l'architecture de notre processeur}
\end{figure}

\subsubsection{Générateur de valeur immédiate}

Selon le type de l'instruction (voir ci-dessous les différents types d'instructions), la valeur immédiate est située à différents endroits dans le code de l'instruction. Le générateur de valeur immédiate récupère les deux bits du code de l'instruction et génère la valeur immédiate de 16 bits par tranches de 4 bits à l'aide de multiplexeurs branchés sur chacun des bits indiquant le type d'instruction. Il l'étend ensuite à 32 bits en reproduisant le bit de signe.

\subsubsection{ALU}

Les opérations arithmétiques et logiques sont entières et réalisées sur 32 bits. L'ALU se compose d'un petit décodeur qui prend 5 bits pour déterminer le nombre d'instructions à effectuer. Il est codé de manière récursive.Les sorties sont le résultat de l'opération et un bit \texttt{zero} qui indique si la condition a été vérifiée, dans le cas des branchements conditionnels.

\subsubsection{Lecture et écriture dans les registres}

On utilise la syntaxe \texttt{x = reg(y)} de minijazz. L'écriture dans les registres se fait uniquement quand le bit \texttt{reg\_write} est à 1.

\subsubsection{Lecture et écriture en mémoire}

On utilise la syntaxe \texttt{ram} et \texttt{rom} de minijazz. Pour \texttt{ram}, l'écriture et la lecture se font en même temps. L'écriture se fait uniquement quand le bit \texttt{mem\_write} est à 1.

\subsubsection{Program counter}

À chaque cycle, l'adresse de l'instruction suivante est calculée par le program counter. En règle générale, il s'agit simplement de l'adresse de l'instruction en cours incrémentée de 1. Le bit 9 du code de l'instruction indique que l'instruction est un saut d'une valeur immédiate relativement à l'adresse de l'instruction en cours. Dans le cas d'un branchement conditionnel, la sortie \texttt{zero} de l'ALU indique si la condition est effectivement vérifiée ; si oui, le saut est effectué. Dans le cas d'un branchement inconditionnel, il faut donner à l'ALU l'adresse de l'instruction en cours en plus de la valeur de l'immédiat, car l'adresse calculée est écrite dans un registre. On force alors la sortie \texttt{zero} de l'ALU à 1 afin que le saut soit effectué dans tous les cas. 

\section{Format des instructions}

\subsection{Types d'instruction}

Toutes les instructions sont codées sur 32 bits. Nous avons pris comme modèle le codage des instructions RISC-V, en le simplifiant. Lorsqu'elles existent, les adresses de lecture et l'adresse d'écriture se trouvent au même endroit dans les différents types d'instruction, et ce afin de simplifier la netlist. Selon le nombre de registres impliqués et leurs rôles respectifs, les instructions sont codées sous différents formats :

\begin{table}[!htbp]
  \centering
  \begin{tabular}{|p{3cm}|p{2cm}|p{2cm}|p{3cm}|p{2.5cm}|}
    \multicolumn{5}{l}{\textbf{Type R :} deux registres source, un registre de destination}\\
    \hline
    [31:24] code instr. & [23:20] rs2 & [19:16] rs1 & [15:12] rd & [11:0] code instr. \\

    \hline \multicolumn{5}{}{}\\
        \multicolumn{5}{l}{\textbf{Type I :} une valeur immédiate (12 bits), un registre source, un registre de destination}\\
    \hline
    \multicolumn{2}{|l|}{[31:20] imm(11:0)} & [19:16] rs1 & [15:12] rd & [11:0] code instr. \\
    \hline \multicolumn{5}{}{}\\
    \multicolumn{5}{l}{\textbf{Type S :} deux registres source, une valeur immédiate (12 bits)}\\
    \hline
    [31:24] imm(11:4) & [23:20] rs2 & [19:16] rs1 & [15:12] imm(3:0) & [11:0] code instr. \\
    \hline \multicolumn{5}{}{}\\
    \multicolumn{5}{l}{\textbf{Type U :} un registre de destination et une valeur immédiate (16 bits)}\\
    \hline
    \multicolumn{3}{|l|}{[31:16] imm(15:0)} & [15:12] rd & [11:0] code instr. \\
    \hline
  \end{tabular}
\end{table}

\subsection{Codage des instructions}

Les instructions sont codées de telle manière à ce que le décodeur d'instruction soit aussi minimal que possible. Presque tous ces bits arrivent directement à l'entrée d'un multiplexeur. 

\begin{table}[!htbp]
  \centering
	\begin{tabular}{|p{3cm}|p{0.5cm}|p{0.5cm}|p{0.5cm}|p{0.5cm}|p{0.5cm}|p{0.5cm}|p{0.5cm}|p{0.5cm}|p{0.5cm}|p{0.5cm}|p{0.5cm}|p{0.5cm}|}
		\hline \textbf{Bit} & 11 & 10 & 9 & 8 & 7 & 6 & 5 & 4 & 3 & 2 & 1 & 0 \\ 
		\hline \textbf{Exemple de \texttt{addi}} & 0 & 1 & 0 & 1 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 1 \\ \hline
  \end{tabular}
\end{table}

\begin{itemize}
	\item \texttt{11} et \texttt{10} : le type d'instruction (R, I, S ou U)
	\item \texttt{9} : \texttt{1} s'il s'agit d'un branchement, \texttt{0} sinon
	\item \texttt{8} : \texttt{1} si l'on écrit dans un registre, \texttt{0} sinon
	\item \texttt{7} : \texttt{1} si l'on écrit en mémoire, \texttt{0} sinon
	\item \texttt{6} : \texttt{1} si l'on lit en mémoire, \texttt{0} sinon
	\item \texttt{5} : \texttt{1} si la source 2 de l'ALU est une valeur immédiate, \texttt{0} s'il s'agit de la valeur lue dans le registre 2
	\item \texttt{4-0} : bits de contrôle de l'ALU, décomposés ainsi :
		\begin{itemize}
			\item \texttt{0} : \texttt{1} s'il s'agit d'une opération logique (bit à bit sans retenue), \texttt{0} s'il s'agit d'une opération arithmétique (avec retenue)
			\item \texttt{1} : s'il s'agit d'une opération logique, \texttt{1} si c'est un \texttt{or}, \texttt{0} si c'est un \texttt{and}. S'il s'agit d'une opération arithmétique, \texttt{0} donne une sortie sur 32 bits et \texttt{1} donne le résultat d'un test sur le bit de poids faible.
			\item \texttt{2} : \texttt{1} pour une soustraction (ça inverse la deuxième entrée et met la retenue initiale à $1$).
			\item \texttt{3} : \texttt{1} pour les entiers signés ou pour le xor (codé comme arithmétique), \texttt{0} sinon.
			\item \texttt{4} : change le sens de l'inégalité lors des comparaisons : \texttt{0} pour $<$ et \texttt{1} pour $\geq$.
		\end{itemize}
\end{itemize}

\section{Jeu d'instructions}

Nous avons choisi d'adapter un sous-ensemble généraliste du jeu d'instructions RISC-V. La liste suivante adopte la syntaxe de notre langage assembleur, très semblable à celle de l'assembleur RISC-V. Les codes d'instruction sont représentés avec le bit de poids faible à droite.

\subsection{Opérations arithmétiques et logiques}

\begin{table}[htbp]
	\centering
	\begin{tabular}{|l|c|c|}
		\hline
		\textbf{Syntaxe} & \textbf{Type} & \textbf{Code de l'instruction} \\
		\hline
		\texttt{add rd, rs1, rs2} & R & 000100000001 \\
		\hline
		\texttt{addi rd, rs1, imm} & I & 010100100001 \\
		\hline
		\texttt{sub rd, rs1, rs2} & R & 000100000101 \\ \hline
		\texttt{subi rd, rs1, imm} & I & 010100100101 \\ \hline
		\texttt{and rd, rs1, rs2} & R & 000100000000 \\ \hline
		\texttt{andi rd, rs1, imm} & I & 010100100000 \\ \hline
		\texttt{or rd, rs1, rs2} & R & 000100000010 \\ \hline
		\texttt{ori rd, rs1, imm} & I & 010100100010 \\ \hline
		\texttt{xor rd, rs1, rs2} & R & 000100001001 \\ \hline
		\texttt{xori rd, rs1, imm} & I & 010100101001 \\ \hline
		\texttt{slt rd, rs1, rs2} & R & 000100000111 \\ \hline
		\texttt{slti rd, rs1, imm} & I & 010100100111\\ \hline
		\texttt{sltu rd, rs1, rs2} & R & 000100001111 \\ \hline
		\texttt{sltiu rd, rs1, imm} & I & 010100101111 \\ \hline
	\end{tabular}
\end{table}

Les instructions \texttt{slt} mettent \texttt{rd} à 1 si \texttt{rs1} $<$ \texttt{rs2/imm} et à 0 sinon. Les opérateurs se terminant par un \texttt{u} désignent une opération non signée.

\subsection{Branchement conditionnel}

Les branchements changent l'adresse de l'instruction suivante à exécuter d'une quantité relative \texttt{imm} par rapport à l'instruction en cours ; cela n'a lieu que si une condition est vérifiée lors de la comparaison des valeurs de deux registres \texttt{rs1} et \texttt{rs2}.

\begin{table}[htbp]
	\centering
	\begin{tabular}{|l|c|c|}
		\hline
		\textbf{Syntaxe} & \textbf{Type} & \textbf{Code de l'instruction} \\ \hline
		\texttt{beq rs1, rs2, imm/LABEL} & S & 111000001101 \\ \hline
		\texttt{bne rs1, rs2, imm/LABEL} & S & 111000000101 \\ \hline
		\texttt{blt rs1, rs2, imm/LABEL} & S & 111000000111 \\ \hline
		\texttt{bltu rs1, rs2, imm/LABEL} & S & 111000001111 \\ \hline
		\texttt{bge rs1, rs2, imm/LABEL} & S & 111000010111 \\ \hline
		\texttt{bgeu rs1, rs2, imm/LABEL} & S & 111000011111 \\ \hline

	\end{tabular}
\end{table}

\subsection{Branchement inconditionnel}

L'instruction \texttt{jal} modifie l'adresse ROM de l'instruction en cours d'exécution et stocke l'adresse de l'instruction suivante dans \texttt{rd}. La valeur immédiate [\texttt{imm}] désigne l'amplitude du saut à effectuer par rapport à l'instruction en cours. L'instruction \texttt{jarl} se comporte comme \texttt{jal} sauf qu'elle lit la valeur du saut relatif dans \texttt{rs2}.

\begin{table}[htbp]
	\centering
	\begin{tabular}{|l|c|c|}
		\hline
		\textbf{Syntaxe} & \textbf{Type} & \textbf{Code de l'instruction} \\ \hline
		\texttt{jal rd, imm/LABEL} & U & 101100100001 \\ \hline
		\texttt{jarl rd, rs2} & R & 001100000001 \\ \hline

	\end{tabular}
\end{table}

\subsection{Lecture et écriture en mémoire}

Les adresses mémoire étant codées sur 16 bits, ne sera prise en compte que la seconde moitié (bits de poids faible) du registre donnant l'adresse d'écriture. 

\begin{itemize}
	\item Pour l'écriture : la valeur immédiate [\texttt{imm}] indique un offset sur l'adresse d'écriture. Les 16 bits de poids faibles du registre source 1 [\texttt{rs1}] donnent l'adresse de lecture. Le registre source 2 [\texttt{rs2}] donne le contenu à écrire.
	\item Pour la lecture : la valeur immédiate [\texttt{imm}] indique un offset sur l'adresse de lecture. Les 16 bits de poids faibles du registre source [\texttt{rs1}] donnent l'adresse de lecture. L'information lue est stockée dans le registre de destination [\texttt{rd}].
\end{itemize}

\begin{table}[htbp]
	\centering
	\begin{tabular}{|l|c|c|}
		\hline
		\textbf{Syntaxe} & \textbf{Type} & \textbf{Code de l'instruction} \\
		\hline \texttt{sw imm(rs1), rs2} & S & 110010100001 \\
		\hline \texttt{lw rd, imm(rs1)} & I & 010101100001 \\ \hline
	\end{tabular}
\end{table}

\subsection{Pseudo-instructions}

Certaines instructions sont traduites vers d'autres instructions avant d'être encodées, en utilisant notamment le fait que \texttt{x0} est toujours à zéro :

\begin{table}[htbp]
	\centering
	\begin{tabular}{|l|c|}
		\hline
		\textbf{Syntaxe} & \textbf{Traduction} \\ 
		\hline \texttt{mv rd, rs1} & \texttt{addi rd, rs1, 0} \\
		\hline \texttt{li rd, imm} & \texttt{addi rd, x0, imm} \\ \hline
		\texttt{j imm/LABEL} & \texttt{jal x0, imm/LABEL} \\ \hline
		\texttt{incr rd} & \texttt{addi rd, rd, 1} \\ \hline
		\texttt{not rd, rs1} & \texttt{xori rd, rs1, -1} \\ \hline
		\texttt{nop} & \texttt{addi x0, x0, 0} \\ \hline
	\end{tabular}
\end{table}

\section{Programme assembleur}

Le programme assembleur a été codé à l'aide des outils d'analyse lexicale et syntaxique d'OCaml, \texttt{ocamllex} et \texttt{menhir}. On lance l'assembleur sur un fichier \texttt{.i} avec la commande \texttt{./iasm [fichier].i} et cela produit un fichier \texttt{.bin}. Les étapes du compilateur sont, classiquement :

\begin{itemize}
	\item \texttt{lexer.mll} reconnaît les opérateurs, les registres et les valeurs immédiates, ainsi que les éléments de syntaxe comme les virgules. 
	\item \texttt{parser.mly} produit un arbre de syntaxe abstraite d'un type défini dans \texttt{ast.ml}. C'est là que les pseudo-instructions sont traduites vers les instructions réelles du processeur.
	\item \texttt{ast.ml} définit une syntaxe abstraite pour un fichier de ce langage, en distingant les différents types d'instruction (R, I, S et U) afin de faciliter la production de code.
	\item \texttt{compile.ml} traduit successivement chaque instruction en son code binaire en traduisant en binaire les numéros des registres et la valeur immédiate si elle existe, et en concaténant chacun de ces éléments ainsi que le code de l'instruction en question dans le bon ordre. Il s'occupe également de traduire une étiquette en une valeur relative en calculant la différence entre le numéro de l'instruction actuelle et le numéro de l'instruction au-dessus de laquelle l'étiquette en question a été définie.
\end{itemize}

La syntaxe de notre langage assembleur est telle que présentée dans les listes d'instructions. On peut également : 
\begin{itemize}
	\item Mettre du code en commentaire en le plaçant entre \texttt{(*} et \texttt{*)}.
	\item Définir une étiquette vers laquelle on pourra sauter, avec la syntaxe \texttt{\#LABEL}.
\end{itemize}

\section{Code de l'horloge}

L'horloge compte les secondes, les minutes et les heures. Les valeurs sont contenues dans les emplacements suivants :

\begin{itemize}
	\item Unités des secondes : \texttt{x1}
	\item Dizaines de secondes : \texttt{x2}
	\item Unités des minutes : \texttt{x3} 
	\item Dizaines de minutes : \texttt{RAM[6]}
	\item Unités des heures : \texttt{RAM[7]}
	\item Dizaines d'heures : \texttt{RAM[8]}
\end{itemize}

Les registres \texttt{x6} à \texttt{x15} contiennent les bitcodes de l'affichage des chiffres de 0 à 9. On procède par incrémentation des bons registres ou des bonnes cases mémoire puis on utilise l'instruction \texttt{jarl} pour sauter d'une valeur relative au contenu du registre ou de la case mémoire afin d'afficher le bon chiffre. Le bitcode du chiffre est écrit dans la RAM à la case correspondante. Les cases 0 à 5 de la RAM sont utilisées pour stocker les bitcodes d'affichage. Ainsi, le programme d'affichage lit le contenu des six premières lignes de la RAM et affiche le chiffre correspondant

\vspace{0.5cm}

Pour augmenter les performances, nous avons créé une version réduite de notre processeur, où : 
\begin{itemize}
	\item Les adresses ROM sont sur 4 bits.
	\item Les adresses RAM sont sur 4 bits. 
	\item Les données de la RAM et des registres sont de taille 16 bits.
\end{itemize}

\end{document}

