open Netlist_ast

let print_only   = ref false
let number_steps = ref (-1)

let rom_file     = ref "rom.data"
let ram_file     = ref "ram.data"
let nsch         = ref false
let nsim         = ref false
let rt           = ref false
let nb_sec_rt    = ref 1

let time_parity  = ref 0

exception WrongInput

let simulator program number_steps =
  (* MEMORY VARIABLES TO STOCK SIMULATION'S CURRENT VALUES *)
  let vars = program.p_vars in
  let vals = ref (Env.map (fun _ -> None) vars) in
  let rom = ref None in
  let ram = ref None in
  (* AUXILIARY FUNCTIONS *)
  (* arg -> value *)
  let arg_value = function
    | Avar i -> Env.find i !vals
    | Aconst c -> Some c
  in
  (* n-th power of 2 *)
  let rec pow_2 = function
    | 0 -> 1
    | n -> 2 * (pow_2 (n-1))
  in
  (* Returns the integer corresponding to a base-two bool array *)
  let arr_to_int a =
    Array.fold_right (fun b s -> 2*s + (Bool.to_int b)) a 0
  in
  (* SIMULATION OF A STEP *)
  let rec simulate_step n =
    let rom_in = open_in !rom_file in
    let ram_in = open_in !ram_file in
    let read_rom () =
      (*let file = open_in "rom.data" in*)
      try
        match String.split_on_char ';' (input_line rom_in(*file*)) with
        | [a_sz; w_sz]
          -> begin
            let n = (*pow_2*) (int_of_string a_sz)
            (*(List.fold_left (fun s b -> 2*s + (int_of_char b)) 0
              (List.of_seq (String.to_seq a_sz)))*) in
            let m = int_of_string w_sz in
            let r = Array.make n (Array.make m false) in
            begin
              try
                for i = 0 to n-1 do
                  let d = Array.map (fun x -> match x with
                                              | '0' -> false
                                              | '1' -> true
                                              | _ -> failwith "Non-bool in ROM")
                                    (Array.of_seq (String.to_seq (input_line rom_in(*file*)))) in
                  if Array.length d != m
                  (*(List.fold_left (fun s b -> 2*s + (int_of_char b)) 0
                    (List.of_seq (String.to_ seq w_sz)))*)
                  then failwith "ROM data format conflict" ;
                  r.(i) <- d
                done
              with End_of_file -> ()
            end;
            rom := Some r
          end
        | _ -> failwith "Unreadable ROM file"
      with End_of_file -> ()
    in
    let read_ram () =
      (*let file = open_in "ram.data" in*)
      try
        match String.split_on_char ';' (input_line ram_in(*file*)) with
        | [a_sz; w_sz]
          -> begin
            let n = (*pow_2*) (int_of_string a_sz)
            (*(List.fold_left (fun s b -> 2*s + (int_of_char b)) 0
              (List.of_seq (String.to_seq a_sz)))*) in
            let m = int_of_string w_sz in
            let r = Array.make n (Array.make m false) in
            begin
              try
                for i = 0 to n-1 do
                  let d = Array.map (fun x -> match x with
                                              | '0' -> false
                                              | '1' -> true
                                              | _ -> failwith "Non-bool in RAM")
                                    (Array.of_seq (String.to_seq (input_line ram_in(*file*)))) in
                  if Array.length d != m
                  (*(List.fold_left (fun s b -> 2*s + (int_of_char b)) 0
                    (List.of_seq (String.to_seq w_sz)))*)
                  then failwith "RAM data format conflict" ;
                  r.(i) <- d
                done
              with End_of_file -> ()
            end;
            ram := Some r
          end
        | _ -> failwith "Unreadable RAM file"
      with End_of_file -> ()
    in
    read_rom () ;
    read_ram () ;
    Printf.printf "Step %d:\n" n;
    (* Variable stocking last-step's values (for registers) *)
    let ex_vals = !vals in
    (* Variable stocking all the information to be written to ram
       at the end of the step (see function write_in_ram below) *)
    let ram_wr = ref [] in
    (* FUNCTIONS FOR SUCCESSIVE ACTIONS IN THE SIMULATION OF A STEP *)
    let rec read_inputs = function
      | [] -> ()
      | i::q -> begin
          Printf.printf "%s ? " i;
          (* Size (in bits) of the expected data *)
          let sz = match Env.find i vars with
            | TBit -> 1
            | TBitArray(s) -> begin Printf.printf "[:%d] " s; s end in
          (* Data entered by the user *)
          let in_value = read_line () in
          try
            if String.length in_value <> sz then raise WrongInput ;
            let bool_of_char = function
              | '0' -> false
              | '1' -> true
              | _ -> raise WrongInput
            in
            vals := Env.update
                      i (fun _
                         -> match sz with
                            | 1 -> Some (Some (VBit
                                                 (bool_of_char
                                                    (String.get in_value 0))))
                            | k -> begin
                                let arr = Array.make k false in
                                String.iteri (fun j c
                                              -> arr.(j) <- bool_of_char c)
                                             in_value ;
                                Some (Some (VBitArray (arr)))
                              end) !vals ;
            read_inputs q
          with WrongInput -> Printf.printf "Wrong imput.\n" ;
                             read_inputs (i::q)
        end
    in
    read_inputs program.p_inputs ;
    let rec simulate_eqs eq_nb = function
      | [] -> ()
      | (ident,exp)::q -> begin
          begin
            match exp with
            | Earg a
              -> vals := Env.update
                           ident (fun _ -> Some (arg_value a)) !vals
            | Ereg x
              -> let y = match Env.find x ex_vals with
                   | Some i -> i
                   | None -> VBit(false) in
                 vals := Env.update
                           ident (fun _ -> Some (Some y)) !vals
            | Enot a
              -> begin
                match arg_value a with
                | Some (VBit b) ->
                   vals := Env.update
                             ident (fun _ ->
                               Some (Some (VBit(not b)))) !vals
                | _ -> Printf.printf
                         "Error [NOT] : The argument must be a well-defined 1-bit boolean.\n"
              end
            | Ebinop (b,a1,a2)
              -> begin
                match arg_value a1, arg_value a2 with
                | Some VBit(b1), Some VBit(b2)
                  -> begin
                    match b with
                    | Or
                      -> vals := Env.update
                                   ident (fun _ ->
                                     Some (Some (VBit(b1 || b2))))
                                   !vals
                    | Xor
                      -> vals := Env.update
                                   ident (fun _ ->
                                     Some (Some(VBit
                                            (b1 && not b2 || not b1 && b2))))
                                   !vals
                    | And
                      -> vals := Env.update
                                   ident (fun _ ->
                                     Some (Some (VBit(b1 && b2))))
                                   !vals
                    | Nand
                      -> vals := Env.update
                                   ident (fun _ ->
                                     Some (Some (VBit
                                                   (not (b1 && b2)))))
                                   !vals
                  end
                | _ -> Printf.printf
                         "Error %d [Binary operation] : Arguments must be well-defined 1-bit booleans.\n" eq_nb
              end
            | Emux (c,a0,a1)
              -> begin
                match arg_value c with
                | Some VBit(b)
                  -> vals := Env.update
                               ident (fun _ ->
                                 Some (if b then arg_value a1
                                      else arg_value a0))
                               !vals
                | _ -> Printf.printf
                         "Error [MUX] : Control bit is not well-defined.\n"
              end
            | Erom (a_sz,w_sz,rd_a)
              -> begin
                match !rom with
                | None -> begin
                    rom := Some (Array.map (fun _ -> Array.make w_sz false)
                                (Array.make (pow_2 a_sz) [||])) ;
                    vals := Env.update
                              ident (fun _ ->
                                Some (Some (VBitArray
                                              (Array.make w_sz false))))
                              !vals
                  end
                | Some r -> begin
                    match arg_value rd_a with
                    | Some (VBitArray(a))
                         when Array.length a = a_sz
                    -> vals := Env.update
                              ident (fun _ ->
                                Some (Some (VBitArray
                                           (r.(arr_to_int a))))) !vals
                    | _ -> Printf.printf
                             "Error [ROM] : Address is not a well-defined array.\n"
                  end
              end
            | Eram (a_sz,w_sz,rd_a,wr_e,wr_a,data)
              -> begin
                begin
                  match !ram with
                  | None -> begin
                      ram := Some (Array.map (fun _ -> Array.make w_sz false)
                                             (Array.make (pow_2 a_sz) [||])) ;
                      vals := Env.update
                                ident (fun _ ->
                                  Some (Some (VBitArray
                                                (Array.make w_sz false))))
                                       !vals
                    end
                  | Some r -> begin
                      match arg_value rd_a with
                      | Some (VBitArray(a))
                           when Array.length a = a_sz
                        -> vals := Env.update
                                     ident (fun _ ->
                                       Some (Some (VBitArray
                                                     (r.(arr_to_int a))))) !vals
                      | _ -> Printf.printf
                               "Error [RAM] : Address to read is not a well-defined array.\n"
                    end
                end ;
                begin
                  match arg_value wr_e with
                  | Some (VBit(b))
                    -> if b then ram_wr := (a_sz,w_sz,wr_a,data)::!ram_wr
                  | _ -> Printf.printf
                           "Error [RAM] : The bit for enabling/desabling writing is not well-defined.\n"
                end
              end
            | Econcat (a1,a2)
              -> begin
                match arg_value a1, arg_value a2 with
                | Some VBit(b), Some VBit(b')
                  -> vals := Env.update
                               ident (fun _ ->
                                 Some (Some (VBitArray([|b;b'|])))) !vals
                | Some VBit(b), Some VBitArray(a')
                  -> vals := Env.update
                               ident (fun _ ->
                                 Some (Some (VBitArray(Array.append [|b|] a'))))
                               !vals
                | Some VBitArray(a), Some VBit(b')
                  -> vals := Env.update
                               ident (fun _ ->
                                 Some (Some (VBitArray(Array.append a [|b'|]))))
                               !vals
                | Some VBitArray(a), Some VBitArray(a')
                  -> vals := Env.update
                               ident (fun _ ->
                                 Some (Some (VBitArray(Array.append a a'))))
                               !vals
                | _ -> Printf.printf
                         "Error %d [CONCAT] : Undefined value.\n" eq_nb
              end
            | Eslice (i1,i2,a)
              -> begin
                match a with
                | Avar id
                  -> begin
                    match Env.find id vars with
                    | TBitArray(s) when s > i2 && i2 >= i1
                      -> begin
                        match Env.find id !vals with
                        | Some (VBitArray(x))
                          -> vals := Env.update
                                       ident (fun _ ->
                                         Some (Some (VBitArray
                                                       (Array.sub x i1 (i2-i1+1)))))
                                       !vals
                        | _ -> Printf.printf "Error %d [SLICE 0] : type problem.\n" eq_nb
                      end
                    | _ -> Printf.printf "Error %d [SLICE 1] : type problem.\n" eq_nb
                  end
                | Aconst VBitArray(x)
                  -> vals := Env.update
                               ident (fun _ ->
                                 Some (Some (VBitArray
                                               (Array.sub x i1 (i2-i1+1)))))
                               !vals
                | _ -> Printf.printf
                         "Error %d [SLICE 2] : A well-defined array is required.\n" eq_nb
              end
            | Eselect (i,a)
              -> begin
                match a with
                | Avar id
                  -> begin
                    match Env.find id vars with
                    | TBitArray(s) when s > i
                      -> begin
                        match Env.find id !vals with
                        | Some (VBitArray(x))
                          -> vals := Env.update
                                       ident (fun _ ->
                                         Some (Some (VBit(x.(i)))))
                                       !vals
                        | _ -> print_string "Error [SELECT] : Type problem.\n"
                      end
                    | TBit when i = 0
                      -> let b = Env.find id !vals in
                         vals := Env.update
                                   ident (fun _ -> Some b) !vals
                    | _ -> print_string "Error [SELECT] : Type problem.\n"
                  end
                | Aconst VBitArray(x)
                  -> vals := Env.update
                               ident (fun _ ->
                                 Some (Some (VBit(x.(i)))))
                               !vals
                | Aconst VBit(b)
                  -> vals := Env.update
                               ident (fun _ ->
                                 Some (Some (VBit(b))))
                               !vals
              end
          end ;
          simulate_eqs (eq_nb + 1) q
        end
    in
    simulate_eqs 0 program.p_eqs ;
    let rec write_in_ram = function
      | [] -> ()
      | (a_sz,w_sz,wr_a,data)::q
        -> begin
          match arg_value wr_a with
          | Some (VBitArray(a))
               when Array.length a = a_sz
            -> begin
              match !ram with
              | Some r -> begin
                  match arg_value data with
                  | Some VBitArray(d)
                    -> r.(arr_to_int a) <- d
                  | None -> Printf.printf
                     "Error [RAM] : Unable to write undefined data.\n"
                  | _ -> Printf.printf
                           "Error [RAM] : Only arrays can be written, not isolated bits.\n"
                end
              | None -> failwith "Internal_error"
            end
          | _ -> Printf.printf
                   "Error [RAM] : Adress to write is not a well-defined array.\n"
        end
    in
    write_in_ram !ram_wr ;
    let rec print_outputs = function
      | [] -> ()
      | i::q -> begin
          begin
            match Env.find i !vals with
            | None -> Printf.printf
                        "Warning : Output value of \"%s\" is not defined.\n" i
            | Some VBit(b)
              -> Printf.printf "=> %s = %d\n" i (Bool.to_int b)
            | Some VBitArray(a)
              -> Printf.printf "=> %s = %s\n" i
                               (Array.fold_left
                                  (fun s b -> if b then s^"1" else s^"0") "" a)
          end ;
            print_outputs q
        end
    in
    print_outputs program.p_outputs ;
    flush stdout ;
    close_in rom_in ;
    close_in ram_in ;
    let ram_out = open_out !ram_file in
    let write_ram () =
      (*let file = open_out "ram.data" in*)
      match !ram with
      | Some r -> begin begin
          if !rt && r.(15).(0) then
            let rec wait () =
              if (int_of_float (Unix.gettimeofday ()))/(!nb_sec_rt) mod 2 = !time_parity then
                begin
                  ignore(Unix.select [] [] [] (0.001)) ;
                  wait ()
                end
              else begin
                  r.(15).(0) <- false ;
                  time_parity := 1- !time_parity
                end
            in wait ()
                    end ;
          Printf.fprintf ram_out(*file*) "%d;%d\n"
                         (Array.length r) (Array.length r.(0))
          (*(Array.fold_left (fun s b -> 10*s + (Bool.to_int b))
            0 (Array.length r))
            (Array.fold_left (fun s b -> 10*s + (Bool.to_int b))
            0 (Array.length r.(0)))*) ;
          Array.iteri (fun i x ->
              Array.iter (fun b -> output_string ram_out (string_of_int (Bool.to_int b))) r.(i);
              output_string ram_out "\n") r
        end
      | None -> ()
    in
    write_ram () ;
    close_out ram_out
  in
  (* Iterating simulation steps *)
  let rec iter_sim k n =
    flush stdout ;
    match n with
    | -1 -> begin
        simulate_step k ;
        iter_sim (k+1) (-1)
      end
    | n when n < k -> ()
    | n -> begin
        simulate_step k ;
        iter_sim (k+1) n
      end
  in
  iter_sim 1 number_steps

let compile filename =
  try
    let p = Netlist.read_file filename in
    begin try
        let p = if !nsch then p else Scheduler.schedule p in
        if !nsim then () else simulator p !number_steps
      with
        | Scheduler.Combinational_cycle ->
            Format.eprintf "The netlist has a combinatory cycle.@.";
    end;
  with
    | Netlist.Parse_error s -> Format.eprintf "An error accurred: %s@." s; exit 2

let main () =
  Arg.parse
    [("-n",   Arg.Set_int number_steps,
              "Number of steps to simulate [default: -1 (infinity)]") ;
     ("-rom", Arg.Set_string rom_file,
              "Path to ROM file [default: rom.data]") ;
     ("-ram", Arg.Set_string ram_file,
              "Path to RAM file [default: ram.data]") ;
     ("-nsch", Arg.Set nsch,
              "Skip netlist scheduler? [default: false]") ;
     ("-nsim", Arg.Set nsim,
              "Skip netlist simulator? [default: false]") ;
     ("-rt",  Arg.Set rt,
              "Real time clock variable rt? [default: false]") ;
     ("-rt_nb_sec",  Arg.Set_int nb_sec_rt,
              "Correction with respect to real time every k seconds [default: 1]")]
    compile
    ""
;;

main ()
