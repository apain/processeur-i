Simulateur de Netlists de Luis KUFFNER.

Le simulateur de Netlists fonctionne de la même façon que celui en exemple pour tester le Scheduler du TP1. Cependant, il est complet et permet de simuler tous les exemples fournis avec le TP.

La compilation s'effectue comme indiqué à la fin du TP.

La difficulté majeure était dans l'appropriation des structures définies dans le squelette fourni, notamment au sujet du typage.
