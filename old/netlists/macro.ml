let _ =
  let n = "100000000000000" (*read_line ()*) in
  let file = "clr" (*read_line ()*) in
  print_int (Sys.command ("../assembleur/iasm ../" ^ file ^ ".i"));
  print_int (Sys.command ("cp ../" ^ file ^ ".bin ./rom.data")) ;
  print_int (Sys.command ("cp ram4.template ram.data")) ;
  print_int (Sys.command ("./sim.byte"
                          ^" -n "^n
                          ^" -rt"
                          ^" -nsch"
                          (*^" -rt_nb_sec 10"*)
                          ^" main_very_fast_sch.net"))
