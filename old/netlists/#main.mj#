(*
- Useful functions
- Immediate generator
- ALU
- Registers
- Program counter
- Memory
- Main
*)

(* == Useful functions == *)

mux_n<n>(b, t:[n], f:[n]) = (out:[n]) where
  if n = 0 then
    out = [];
  else
    mux0 = mux(b, t[0], f[0]);
    muxn = mux_n<n-1>(b, t[1..], f[1..]);
    out = mux0 . muxn;
  end if
end where

mux_4(b, t:[4], f:[4]) = (out:[4]) where
  out = mux_n<4>(b, t, f)
end where

mux_16(b, t:[16], f:[16]) = (out:[16]) where
  out = mux_n<16>(b, t, f)
end where

mux_32(b, t:[32], f:[32]) = (out:[32]) where
  out = mux_n<32>(b, t, f)
end where

get_16(imm:[32]) = (imm_16:[16]) where
  imm_16 = imm[0..15]
end where

get_6(imm:[32]) = (imm_6:[6]) where
  imm_6 = imm[0..5]
end where

sign_extend_16(a_16:[16]) = (a_32:[32]) where
  f = a_16[15] ;
  a_32 = a_16 . f . f . f . f . f . f . f . f . f . f . f . f . f . f . f . f
end where

(* == IMMEDIATE GENERATOR == *)

imm_gen(instr:[32], type_instr:[2]) = (imm:[32]) where
  t_0 = type_instr[0] ;
  t_1 = type_instr[1] ;
  s = instr[31] ;
  sign4 = s . s . s . s ;
  imm15_12 = mux_4(t_0, instr[28..31], sign4);
  imm11_8 = mux_4(t_0, instr[24..27], instr[28..31]);
  imm7_4 = mux_4(t_0, instr[20..23], instr[24..27]);
  imm3_0 = mux_4(t_0, instr[16..19], mux_4(t_1, instr[20..23], instr[12..15]));

  imm = sign_extend_16(imm3_0 . imm7_4 . imm11_8 . imm15_12)

end where

(* == ALU == *)

alu_control(c_0, c_1, c_2, c_3, c_4) = (c_in, b_inv, c_inhib, and_or, plus_zero, log_arith) where
  log_arith = c_0 ;
  plus_zero = c_1 ;
  and_or = c_1 ;
  c_inhib = not c_0 + c_3 & not c_2 & not c_1 ;
  b_inv = c_2 ;
  c_in = c_2
end where

alu_rec<n>(a:[n], b:[n], c_in, b_inv, c_inhib, and_or, plus_zero, log_arith) = (o:[n], plus_f, c_out, nz) where
  if n = 0 then
    o = [] ;
    plus_f = 0 ;
    c_out = c_in ;
    nz = 0
  else
    (o_p, plus_p, c_p, nz_p) = alu_rec<n-1>(a[..n-2], b[..n-2], c_in, b_inv, c_inhib, and_or, plus_zero, log_arith) ;
    a_f = a[n-1] ;
    b_f = b[n-1] ^ b_inv ;
    c_pp = mux (c_inhib, c_p, 0) ;
    and_f = a_f & b_f ;
    or_f = a_f + b_f ;
    plus_f = (a_f ^ b_f) ^ c_pp ;
    log_f = mux (and_or, and_f, or_f) ;
    arith_f = mux (plus_zero, plus_f, 0) ;
    o_f = mux (log_arith, log_f, arith_f) ;
    c_out = and_f + c_p & or_f ;
    o = o_p . o_f ;
    nz = o_f + nz_p
  end if
end where

alu(a:[32], b:[32], c:[5]) = (o:[32], bch) where
  c_0 = c[0] ;
  c_1 = c[1] ;
  c_2 = c[2] ;
  c_3 = c[3] ;
  c_4 = c[4] ;
  (c_in, b_inv, c_inhib, and_or, plus_zero, log_arith) = alu_control(c_0, c_1, c_2, c_3, c_4) ;
  (o_alu, plus_0, c_out, nz_alu) = alu_rec<32>(a, b, c_in, b_inv, c_inhib, and_or, plus_zero, log_arith) ;
  sl = c_0 & c_1 ;
  u = c_3 ;
  a_f = a[31] ;
  b_f = b[31] ;
  pre_comp = ((u & not c_out) + (not u & (a_f & not b_f + not (c_out + (a_f ^ b_f))))) ;
  comp = mux(c_4, pre_comp, not pre_comp) ;
  o_0 = o_alu[0] ;
  o_1 = o_alu[1..] ;
  o_0f = (sl & comp) + (not sl & o_0) ;
  o = o_0f . o_1 ;
  nz = (sl & comp) + (not sl & nz_alu) ;
  bch = ((not c_1) & (nz ^ u) + c_1 & comp)
end where

(* == REGISTERS == *)

registers_aux<n, i>(rs1_addr:[n], rs2_addr:[n], wreg_en, rdest_addr:[n], data:[i]) = (rs1:[i], rs2:[i]) where
  if n = 0 then
    if i = 0 then
      rs1 = [] ;
      rs2 = []
    else
      (rs1_i, rs2_i) = registers_aux<n, i-1>(rs1_addr, rs2_addr, wreg_en, rdest_addr, data[1..]) ;
      x = reg(mux(wreg_en, x, data[0])) ;
      rs1 = x . rs1_i ;
      rs2 = x . rs2_i
    end if
  else
    rdest_a0 = rdest_addr[0] ;
    rs1_a0 = rs1_addr[0] ;
    rs2_a0 = rs2_addr[0] ;
    (rs1_rec_0, rs2_rec_0) = registers_aux<n-1, i>(rs1_addr[1..], rs2_addr[1..], wreg_en & not rdest_a0, rdest_addr[1..], data) ;
    (rs1_rec_1, rs2_rec_1) = registers_aux<n-1, i>(rs1_addr[1..], rs2_addr[1..], wreg_en & rdest_a0, rdest_addr[1..], data) ;
    rs1 = mux_32(rs1_a0, rs1_rec_0, rs1_rec_1) ;
    rs2 = mux_32(rs2_a0, rs2_rec_0, rs2_rec_1)
  end if
end where

registers(rs1_addr:[4], rs2_addr:[4], wreg_en, rdest_addr:[4], data:[32]) = (rs1:[32], rs2:[32]) where
  w_en = wreg_en & (rdest_addr[0] + rdest_addr[1] + rdest_addr[2] + rdest_addr[3]) ;
  (rs1, rs2) = registers_aux<4, 32>(rs1_addr, rs2_addr, w_en, rdest_addr, data)
end where

(* == PROGRAM COUNTER == *)

(* Adder pour les adresses de lecture *)

fulladder(a, b, c_in) = (s, c_out) where
	t = a ^ b ;
	s = t ^ c_in ;
	c_out = (a & b) + (t & c_in) ;
end where

add_n<n>(a:[n], b:[n], c_in) = (s:[n], c_out) where
	if n = 0 then
		s = [] ;
		c_out = 0 ;
	else
		(s_0, c_0) = fulladder(a[0], b[0], c_in) ;
		(s_1, c_out) = add_n<n-1>(a[1..], b[1..], c_0) ;
		s = s_0 . s_1 ;
	end if
end where

add_16(a:[16], b:[16]) = (s:[16]) where
	(s, c_out) = add_n<16>(a, b, 0) ;
end where

incr(program_addr:[16]) = (new_addr:[16]) where
  one_16 = 1 . 0 . 0 . 0 . 0 . 0 . 0 . 0 . 0 . 0 . 0 . 0 . 0 . 0 . 0 . 0 ;
  new_addr = add_16(program_addr, one_16)
end where

jump_addr(program_addr:[16], instr:[32], imm:[32] (*, zero*)) = (new_addr:[16]) where
  imm_jump = get_16(imm);
  new_addr (*_branch1*) = add_16(program_addr, imm_jump);
  (*new_addr = mux_16(zero, incr(program_addr), new_addr_branch1)*)
end where
(* check that zero bit is at 1 when result is 0 *)

new_program_addr(program_addr:[16], instr:[32], bch, imm:[32]) = (new_addr:[16]) where
  new_addr = mux_16(bch, incr(program_addr), jump_addr(program_addr, instr, imm))
end where

(* == MEMORY READ/WRITE == *)

memory(addr_ext:[32], write_enable, write_data:[32]) = (read_data:[32]) where
  addr = get_6(addr_ext) ;
  read_data = ram<6, 32>(addr, write_enable, addr, write_data)
end where

get_instr(program_addr:[16]) = (instr:[32]) where
  instr = rom<16, 32>(program_addr)
end where
(* MODIFY: access ROM *)

(* == MAIN == *)

main_bis(program_addr:[16]) = (next_addr:[16], o:[32], rs1:[4], rs2:[4], rdest:[4], data:[32]) where
  instr = get_instr(program_addr);
  alu_control = instr[0..4];
  alu_src = instr[5];
  mem_read = instr[6];
  mem_write = instr[7];
  reg_write = instr[8];
  branch = instr[9];
  type_instr = instr[10..11];
  rdest = instr[12..15];
  rs1 = instr[16..19];
  rs2 = instr[20..23];
  imm = imm_gen(instr, type_instr);
  (a_0, b_0) = registers(rs1, rs2, reg_write, rdest, data) ;
  bch_inc = (not alu_control[2]) & branch ;
  a = mux_32(bch_inc, a_0, sign_extend_16(program_addr));
  b = mux_32(alu_src, b_0, imm);
  (o, alu_branch) = alu(a, b, alu_control);
  data = mux_32(mem_read, o, memory(o, mem_write, b_0));

  bch_f = branch & (not bch_inc & alu_branch + bch_inc) ;
  next_addr = new_program_addr(program_addr, instr, bch_f, mux_32(type_instr[1], b_0, imm))
end where

main() = (o:[32], rs1:[4], rs2:[4], rdest:[4], data:[32]) where
  addr = reg(a_0) . reg(a_1) . reg(a_2) . reg(a_3) . reg(a_4) . reg(a_5) . reg(a_6) . reg(a_7) . reg(a_8) . reg(a_9) . reg(a_10) . reg(a_11) . reg(a_12) . reg(a_13) . reg(a_14) . reg(a_15) ;
  (nxt, o, rs1, rs2, rdest, data) = main_bis(addr) ;
  a_0 = nxt[0] ;
  a_1 = nxt[1] ;
  a_2 = nxt[2] ;
  a_3 = nxt[3] ;
  a_4 = nxt[4] ;
  a_5 = nxt[5] ;
  a_6 = nxt[6] ;
  a_7 = nxt[7] ;
  a_8 = nxt[8] ;
  a_9 = nxt[9] ;
  a_10 = nxt[10] ;
  a_11 = nxt[11] ;
  a_12 = nxt[12] ;
  a_13 = nxt[13] ;
  a_14 = nxt[14] ;
  a_15 = nxt[15]
end where
