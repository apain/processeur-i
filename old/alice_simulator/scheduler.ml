open Netlist_ast
open Graph

exception Combinational_cycle

let read_exp eq = 
        (* returns a list of the input values of an equation *)
        let to_id a = match a with
        | Avar id -> [id]
        | _ -> []
        in
        let _, e = eq in
        match e with
        | Earg x | Enot x | Eslice (_, _, x) | Eselect (_, x) | Erom (_, _, x) | Eram (_, _, x, _, _, _) -> to_id x
        | Ereg _ -> []
        | Ebinop (_, x, y) | Econcat (x, y) -> to_id x@to_id y
        | Emux (x, y, z) -> to_id x@to_id y@to_id z

let schedule p = 
        (* creates a dependency graph from a given net-list and orders it topologically *)
        let g = mk_graph () in
        let rec add_nodes l =
                match l with
                | [] -> ()
                | (i, _)::t -> 
                        add_node g i; 
                        add_nodes t
        in
        add_nodes p.p_eqs;

        let g_labels = List.map (fun x -> x.n_label) g.g_nodes in
        let rec add_edges l = 
                match l with
                | [] -> ()
                | h::t -> let i, e = h in
                List.iter (function x -> if List.mem x g_labels then add_edge g x i) (read_exp h);
                add_edges t
        in 
        add_edges p.p_eqs;
        if has_cycle g then raise Combinational_cycle;
        let sorted_list = topological g in
        let rec sort_eqs sorted_list =
                match sorted_list with
                | [] -> []
                | h::t -> (h, List.assoc h p.p_eqs)::sort_eqs t
        in 
        {
                p_eqs = sort_eqs sorted_list;
                p_inputs = p.p_inputs;
                p_outputs = p.p_outputs;
                p_vars = p.p_vars;
        }
