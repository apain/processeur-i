open Netlist_ast
open Tools

let print_only = ref false
let number_steps = ref 100

type full_env = { vars : value Env.t ;
                regs : value Env.t ;
                rom : value array Env.t ;
                ram : value array Env.t }

(* ROM/RAM *)
let mem_arr addr_size word_size =
        (* returns an array of size 2^add_size containing bit arrays of length word_size *)
        let size = 1 lsl addr_size in
        let init_arrs = Array.make word_size false in
        Array.make size (VBitArray (init_arrs))

let init_rom_ram program =
         List.fold_left (fun env (id, exp) -> 
                begin match exp with 
                | Erom (addr_size, word_size, _) -> let rom_arr = mem_arr addr_size word_size in 
                        { vars = env.vars ; regs = env.regs ; rom = Env.add id rom_arr env.rom ; ram = env.ram }
                | Eram (addr_size, word_size, _, _, _, _) -> let ram_arr = mem_arr addr_size word_size in 
                        { vars = env.vars ; regs = env.regs ; rom = env.rom ; ram = Env.add id ram_arr env.ram }
                | _ -> env
                end
         ) { vars = Env.empty ; regs = Env.empty ; rom = Env.empty ; ram = Env.empty } program.p_eqs

let read_rom id env addr = 
        let int_addr = int_of_arr addr in
        let rom_arr = Env.find id env.rom in
        rom_arr.(int_addr)

let read_ram id env addr = 
        let int_addr = int_of_arr addr in
        let ram_arr = Env.find id env.ram in
        ram_arr.(int_addr)

(* Accessing variables *)
let get_var env id =
        try (Env.find id env.vars) with Not_found -> Format.eprintf "Error: Unbound value %s \n" id; exit 2

let get_reg env id = 
        try (Env.find id env.regs) with Not_found -> VBit false

(* Evaluation *)
let eval_arg env arg = 
        match arg with
        | Avar id -> get_var env id
        | Aconst v -> v

let eval_exp env id exp = 
        match exp with
        | Earg arg -> eval_arg env arg
        | Ereg _ -> get_reg env id
        | Enot arg -> let b = get_bool (eval_arg env arg) in
                VBit (not b)
        | Ebinop (binop, arg1, arg2) ->
                        let b1 = get_bool (eval_arg env arg1) in
                        let b2 = get_bool (eval_arg env arg2) in
                        VBit (match binop with
                                | Or -> b1 || b2
                                | And -> b1 && b2
                                | Xor -> (b1 && not b2) || (not b1 && b2)
                                | Nand -> not (b1 && b2))
        | Emux (arg1, arg2, arg3) -> 
                        let b = get_bool (eval_arg env arg1) in
                        if not b then eval_arg env arg2
                        else eval_arg env arg3
        | Erom (_, _, arg) ->
                        let addr = get_array (eval_arg env arg) in
                        read_rom id env addr
        | Eram (_, _, arg, _, _, _) ->
                        let addr = get_array (eval_arg env arg) in
                        read_ram id env addr
        | Econcat (arg1, arg2) -> 
                let e1 = eval_arg env arg1 in 
                let e2 = eval_arg env arg2 in
                let arr1 = get_array e1 in
                let arr2 = get_array e2 in
                VBitArray (Array.concat [arr1; arr2])
        | Eslice (i, j, arg) -> 
                let e = eval_arg env arg in
                let arr = get_array e in
                VBitArray (Array.sub arr i (j-i+1))
        | Eselect (i, arg) -> 
                let e = eval_arg env arg in
                let arr = get_array e in VBit arr.(i)

let add_value (id, exp) env =
        Env.add id (eval_exp env id exp) env.vars
        
let eval_exps env program =
        List.fold_left (fun env eq -> { vars = add_value eq env ; regs = env.regs ; rom = env.rom ; ram = env.ram } ) env program.p_eqs

(* Writing *)
let write_ram_regs env program =
        List.fold_left ( fun env (id, exp) ->
                begin match exp with
                | Ereg x -> let v = Env.find x env.vars in { vars = env.vars ; regs = Env.add id v env.regs ; rom = env.rom ; ram = env.ram }
                | Eram (_, _, _, we, arg1, arg2) ->
                        let b = get_bool (eval_arg env we) in 
                        if b then let addr = int_of_arr (get_array (eval_arg env arg1)) in
                                let data = eval_arg env arg2 in 
                                let ram_arr = Env.find id env.ram in 
                                ram_arr.(addr) <- data; 
                                env
                        else env
                | _ -> env
                end
        ) 
        env 
        program.p_eqs

(* Simulator *)
let get_user_inputs program =
        let rec get_values l =
                match l with
                | [] -> []
                | h::t as l -> 
                        let input_type = Env.find h program.p_vars in
                        let n = get_len input_type in
                        print_string h; Printf.printf " [:%d] = ? " n;
                        let input = read_line () in
                        begin match input_type with
                        | TBit -> 
                                if String.length input = 1 then 
                                        try (let x = bool_of_char input.[0] in let v = VBit x in (h, v)::get_values t) 
                                        with Invalid_input -> Printf.printf "Invalid input. Possible values are 0 and 1. \n"; get_values l
                                else get_values l
                        | TBitArray n -> 
                                if String.length input = n then
                                        try (let v = VBitArray (barray_of_string input) in (h, v)::get_values t) with Invalid_input -> Printf.printf "Invalid input. Expected input is bit-array of size %d. \n" n; get_values l
                                else get_values l
                        end
        in get_values program.p_inputs

let simulate_step env program =
        let user_inputs = get_user_inputs program in (* get user inputs *)
        let input_map = Env.of_list user_inputs in
        let full_env = { vars = input_map ; regs = env.regs ; rom = env.rom ; ram = env.ram } in
        let eval_env = eval_exps full_env program in (* evaluate equations *)
        let new_env = write_ram_regs eval_env program in (* write in ram and registers *)
        List.iter (fun id -> let value = Env.find id new_env.vars in
        Printf.printf "=> %s = %s\n" id @@ string_of_value value) program.p_outputs;
        new_env (* returns the updated environment *)

let simulator program number_steps =
        let empty_env = init_rom_ram program in
        let previous_step = ref empty_env in
        let loop = ref 0 in
        while !loop <> number_steps do
                incr loop;
                Printf.printf "Step %d: \n" !loop;
                previous_step := simulate_step !previous_step program;
        done

let compile filename =
        try
        let p = Netlist.read_file filename in
                begin try
                        let p = Scheduler.schedule p in
                        if !print_only then Netlist_printer.print_program stdout p
                        else simulator p !number_steps
                 with
                | Scheduler.Combinational_cycle ->
                        Format.eprintf "The netlist has a combinatory cycle.@.";
                end;
        with
        | Netlist.Parse_error s -> Format.eprintf "An error occurred: %s@." s; exit 2

let main () =
        Arg.parse
                ["-print", Arg.Set print_only, "Print the ordered netlist on standard output";
                "-n", Arg.Set_int number_steps, "Number of steps to simulate. Default is 1."]
                compile
                ""
;;

main ()

